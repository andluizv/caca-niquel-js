const btn1 = document.querySelector('#btn1');
const btn2 = document.querySelector('#btn2');
const btn3 = document.querySelector('#btn3');
const btnApostar = document.querySelector('#btn-apostar');
const msgInicial = document.querySelector('.msg');
const roda1 = document.querySelector('#roda1');
const roda2 = document.querySelector('#roda2');
const roda3 = document.querySelector('#roda3');
const countAmount = document.querySelector('#conta');
const mostraValorApostado = document.querySelector('#qual-aposta');
const mostraPremiacao = document.querySelector('#cash-back');
let recebeAposta = 0;
const list = ['zero', 'um', 'dois', 'tres', 'quatro', 'cinco', 'seis']
let posição1 = 0;
let posição2 = 0;
let posição3 = 0;
let numGerado = 0;


const getRandom = () => {
    n = Math.random() * 5 + 1;
    return Math.round(n);
}
const paraRoda = (numRoda) => {
    let formaClass = document.querySelector(`#roda${numRoda}`);
    numGerado = getRandom();
    formaClass.classList.remove('--rotate');
    
   
    for (let i = 1; i < 7; i++){
        hide = document.querySelector(`#roda${numRoda} > .${list[i]}`);
        hide.classList.add('--hidde');
    }
    placa = document.querySelector(`#roda${numRoda} .${list[numGerado]}`);
    
    placa.style.display = 'grid';
    placa.style.position = 'relative';
    placa.style.transform = 'none';
    placa.style.position = 'relative';
    placa.style.alignSelf = 'flex-start';
    placa.style.borderTop = 'none';
    document.querySelector(`#btn${numRoda}`).disabled = true;
    return numGerado;
}
const reset = () => {
    for (let j = 1; j < 4; j++){
        for (let i = 1; i < 7; i++){
        hide = document.querySelector(`#roda${j} > .${list[i]}`);
        hide.classList.remove('--hidde');
        hide.style.display = '';
        hide.style.position = '';
        hide.style.transform = '';
        hide.style.position = '';
        hide.style.alignSelf = '';
        hide.style.borderTop = '';
    }
    document.querySelector(`#btn${j}`).disabled = false;
    }
    msgInicial.classList.add('--hidde');
    roda1.classList.add('--rotate');
    roda2.classList.add('--rotate');
    roda3.classList.add('--rotate');
}
const pegaAposta = () => {
    recebeAposta = document.querySelector('#valor-aposta').value;
    return recebeAposta;
}
const apostManager = () => {
    pegaAposta();
    mostraValorApostado.textContent = recebeAposta + ' moedas.';
}
const calculaPremio = (aposta) => {
    let fator = 0;
    let premio = 0;
    
    if (posição2 == posição1 + 1 && posição3 == posição2 +1){
        fator = 20;
        premio = aposta * fator;
        return premio;
    }
    if (posição1 == posição2 && posição1 == posição3){
        fator = posição1;
        premio = aposta * fator;
        return premio;
    }
    if (posição1 == posição2 || posição1 == posição3){
        return premio = aposta;
    }
    if (posição2 == posição3){
        return premio = aposta;
    }
    return premio;
}
const pegaContaValor = () => {
    return countAmount.textContent;
}
const valuesMecPre = () => {
    let aposta = pegaAposta();
    if (aposta < 0 || aposta == undefined || aposta == ''){
        alert('Selecione um valor válido para aposta!');
        return;
    }
    let atual = Number(pegaContaValor());
    if (aposta > atual){
        if(atual < 10){
            alert('Você está sem moedas, atualize a página para começar do zero!');
            return ;
        }
        alert('Você não tem moedas para essa aposta! Diminua de acordo com o que você tem na conta!');
        return;
    }
    countAmount.textContent = atual - aposta;
    apostManager();
    reset();
}
btn1.addEventListener('click', () => {
    paraRoda(1);
    posição1 = numGerado;
    if (btn2.disabled == true && btn3.disabled == true){
        mostraPremiacao.textContent = calculaPremio(pegaAposta()) + ' moedas';
        countAmount.textContent = Number(countAmount.textContent) + Number(calculaPremio(pegaAposta()));
    }
})
btn2.addEventListener('click', () => {
    paraRoda(2);
    posição2 = numGerado;
    if (btn1.disabled == true && btn3.disabled == true){
        mostraPremiacao.textContent = calculaPremio(pegaAposta()) + ' moedas';
        countAmount.textContent = Number(countAmount.textContent) + Number(calculaPremio(pegaAposta()));
    }
})
btn3.addEventListener('click', () => {
    paraRoda(3);
    posição3 = numGerado;
    if (btn2.disabled == true && btn1.disabled == true){
        a = calculaPremio(pegaAposta());
        mostraPremiacao.textContent = a + ' moedas';
        countAmount.textContent = Number(countAmount.textContent) + Number(calculaPremio(pegaAposta()));
    }
})
btnApostar.addEventListener('click', () => {
    valuesMecPre();
})
